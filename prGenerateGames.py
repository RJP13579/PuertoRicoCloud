"""
Input "numOfGames" and "numOfPlayers", will output JSON messages to an SQS queue.
Each JSON message will be the trigger for 1 game of Puerto Rico
"""
import json
import boto3
import random

def lambda_handler(event, context):
    dyndb = boto3.client('dynamodb')
    sqs = boto3.client('sqs')
    
    #1. Parse & validate query strig params
    numOfGames = event['queryStringParameters']['numOfGames']
    numOfPlayers = event['queryStringParameters']['numOfPlayers']
    httpMethod = event['httpMethod']
    
    status = 'SUCCESS'
    targetQueueUrl = 'https://sqs.eu-west-1.amazonaws.com/653875496653/prGameQueue'
    message = 'This is a test message!'
    
    if not checkInt(numOfGames) or not checkInt(numOfPlayers):
        httpMethod = 'FAIL'
        status = 'ERROR'
        message = 'numOfGames is {}; numOfPlayers is {} - Error, they must both be integers'.format(numOfGames, numOfPlayers)
    if int(numOfPlayers) not in [3,4,5]:
        httpMethod = 'FAIL'
        status = 'ERROR'
        message = "Puerto Rico can only have 3, 4 or 5 Players.  {} invalid number of players.".format(numOfPlayers)

    # Adjust to the correct date types
    numOfGames = int(numOfGames)
    numOfPlayers = int(numOfPlayers)

    #2. Pick a unique selection of players from DynamoDB
    dynDBResponse = scanPlayers(dyndb)
    allPlayers = dynDBResponse['Items']
    playerList = []
    for i in range(int(numOfPlayers)):
        random.shuffle(allPlayers)
        playerList.append(allPlayers.pop()['name']['S'])
    
    # Log a new game to DynamoDB:
    gameId = getGameId(dyndb)
    logNewGame(dyndb, gameId, playerList)
    
    # Send the list of Players to an SQS queue:
    sqsResponse = sendSqsMessage(sqs, targetQueueUrl, gameId, playerList)
        
    message = 'playerList is ' + str(playerList)
    print('playerList is ' + str(playerList))

    #3. Construct the body of the response object
    transactionResponse = {}
    transactionResponse['status'] = status
    transactionResponse['playerList'] = playerList
    transactionResponse['gameId'] = gameId
    # Use message for debugging:
    print(message)

    #4. Construct http response object
    responseObject = {}
    responseObject['statusCode'] = 200
    responseObject['headers'] = {}
    responseObject['headers']['Content-type'] = 'application/json'
    responseObject['body'] = json.dumps(transactionResponse)

    #5. Return the response object
    return responseObject
    

def checkInt(x):
    try:
        int(x)
        return True
    except ValueError:
        return False
        
def scanPlayers(dyndb):
    response = dyndb.scan(
        TableName = 'prPlayers',
        AttributesToGet = ['name']
    )
    return response
    
def getGameId(dyndb):
    response = dyndb.scan(
        TableName = 'prGames',
        AttributesToGet = ['id'] 
    )
    allIds = [int(i['id']['N']) for i in response['Items']]
    maxId = max(allIds) + 1
    return str(maxId)
    
def logNewGame(dyndb, gameId, playerList):
    item = {}
    item['id'] = {'N':gameId}
    item['players'] = {'S':str(playerList)}
    item['status'] = {'S':'In Progress'}
    item['winner'] = {'S':'No winner yet'}
    response = dyndb.put_item(
        TableName = 'prGames',
        Item = item)
        
    
def sendSqsMessage(sqs, targetQueueUrl, gameId, playerList):
    sqsResponse = sqs.send_message(
        QueueUrl = targetQueueUrl,
        DelaySeconds = 10,
        MessageAttributes = {
            'gameId': {
                'DataType': 'Number',
                'StringValue': gameId
            },
            'playerList': {
                'DataType': 'String',
                'StringValue': str(playerList)
                }
            },
        MessageBody = ('This is the message body')
    )
