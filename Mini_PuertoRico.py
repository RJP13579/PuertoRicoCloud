import json
import boto3
from ast import literal_eval
from Mini_Class_PlayerTest import Player
from Mini_Func_PlayGame import PlayGame

sqs = boto3.client('sqs')
s3 = boto3.resource("s3")

def lambda_handler(event, context):
    queue_url = 'https://sqs.eu-west-1.amazonaws.com/653875496653/prGameQueue'

    # -----------------------------------------------
    # Receive, parse & delete message from SQS queue

    # Use the following code to read SQS 'manually' from the queue:
    """
    response = sqs.receive_message(
        QueueUrl=queue_url,
        AttributeNames=[
            'SentTimestamp'
        ],
        MaxNumberOfMessages=1,
        MessageAttributeNames=[
            'All'
        ],
        VisibilityTimeout=0,
        WaitTimeSeconds=0
    )
    print(response)
    fullMessage = response['Messages'][0]           
    sqsPayload = fullMessage['MessageAttributes']
    gameId = sqsPayload['gameId']['StringValue']
    playerNames = literal_eval(sqsPayload['playerList']['StringValue'])
    numOfPlayers = len(playerNames)
    receipt_handle = fullMessage['ReceiptHandle']
    """
    
    # Use the following code to read SQS when it's part of the Lambda trigger:
    fullMessage = event['Records'][0]
    sqsPayload = fullMessage['messageAttributes']
    gameId = sqsPayload['gameId']['stringValue']
    playerNames = literal_eval(sqsPayload['playerList']['stringValue'])
    numOfPlayers = len(playerNames)
    receipt_handle = fullMessage['receiptHandle']
    
    status = 'Message Recieved'
    print(status)

    # Delete received message from queue
    sqs.delete_message(
        QueueUrl=queue_url,
        ReceiptHandle=receipt_handle
    )
    status = status + ' and Deleted'
    print(status)
    print('\n------------')
    print('gameId is: {}'.format(gameId))
    print('numOfPlayers is: {}'.format(numOfPlayers))
    print('playerNames is: {}'.format(playerNames))
    print('------------')
    
    # -----------------------------------------------
    # 2:  Enact the Mini-PuertoRico game
    print('\n------------')
    print('Now playing Puerto Rico:')
    finalScores = PuertoRico(numOfPlayers, playerNames)
    print('Final Scores are: {}'.format(finalScores))
    print('\n------------')

    # -----------------------------------------------
    # 3. Write the outpur to s3
    string = json.dumps(finalScores)
    string = 'Game ' + gameId + ' complete!  The final scores are: ' + string
    encoded_string = string.encode("utf-8")
    bucket_name = 'pr-outputdata'
    file_name = 'game-'+gameId+'-prOutputLog.txt'
    lambda_path = 'tmp/' + file_name
    s3_path = 'output' + file_name
    s3.Bucket(bucket_name).put_object(Key = s3_path, Body = encoded_string)

    
    # -----------------------------------------------
    #4. Construct the body of the response object
    transactionResponse = {}
    transactionResponse['status'] = status
    transactionResponse['Full Message'] = fullMessage
    transactionResponse['sqsPayload'] = sqsPayload
    transactionResponse['finalScores'] = finalScores

    # -----------------------------------------------
    #5. Construct http response object
    responseObject = {}
    responseObject['statusCode'] = 200
    responseObject['headers'] = {}
    responseObject['headers']['Content-type'] = 'application/json'
    responseObject['body'] = json.dumps(transactionResponse)

    # -----------------------------------------------
    #5. Return the response object
    return responseObject
    
    
def PuertoRico(numOfPlayers, playerNames):
    # Create all the players
    x = ['p1','p2','p3','p4','p5']
    allPlayers = []
    for i in range(numOfPlayers):
        x[i] = Player(playerNames[i])
        allPlayers.append(x[i])
    del(x)
    
    finalScores = PlayGame(allPlayers)
    
    return finalScores