"""
Will Create / Update / Retrieve / Delete items from the prPlayer table in DynamoDB
"""
import json
import boto3

def lambda_handler(event, context):
    dyndb = boto3.client('dynamodb')
    
    #1. Parse & validate query strig params
    playerName = event['queryStringParameters']['playerName']
    numOfGames = event['queryStringParameters']['numOfGames'] if 'numOfGames' in event['queryStringParameters'] else '0'
    numOfWins = event['queryStringParameters']['numOfWins'] if 'numOfWins' in event['queryStringParameters'] else '0'
    status = 'SUCESS'
    httpMethod = event['httpMethod']
    
    if not checkInt(numOfGames) or not checkInt(numOfWins):
        httpMethod = 'FAIL'
        status = 'ERROR'
        message = 'numOfGames is {}; numOfWins is {} - Error, they must both be integers'.format(numOfGames, numOfWins)
    else:
        if int(numOfWins) > int(numOfGames):
            httpMethod = 'FAIL'
            status = 'ERROR'
            message = "Player has won {} games, but they only played {}!".format(numOfWins, numOfGames)


    #2. Perform functionality, depending on method:
    if httpMethod == 'GET':
        response = getPlayer(dyndb, playerName)
        if 'Item' in response:
            message = str(response['Item'])
        else:
            status = 'ERROR'
            message = 'GET Unsucessful - {} does not exist in table'.format(playerName)
        
    elif httpMethod == 'POST':
        # Check if Player already exists:
        response = getPlayer(dyndb, playerName)
        if 'Item' in response:
            status = 'ERROR'
            message = 'POST Unsucessful, player {} already exists'.format(playerName)
        else:
            # Insert the new player:
            response = upsertPlayer(dyndb, playerName, numOfGames, numOfWins)
            message = 'Created {} as a new player'.format(playerName)
    
    elif httpMethod == 'PUT':
        # Upsert the new player:
        response = upsertPlayer(dyndb, playerName, numOfGames, numOfWins)
        message = 'Upserted player {}'.format(playerName)
        
    elif httpMethod == 'DELETE':
        # Check if Player already exists:
        response = getPlayer(dyndb, playerName)
        if 'Item' in response:
            response = deletePlayer(dyndb, playerName)
            message = 'Player {} sucessfully deleted'.format(playerName)
        else:
            status = 'ERROR'
            message = 'DELETE Unsucessful, player {} does not exist'.format(playerName)


    #3. Construct the body of the response object
    transactionResponse = {}
    transactionResponse['playerName'] = playerName
    transactionResponse['status'] = status
    transactionResponse['message'] = message

    #4. Construct http response object
    responseObject = {}
    responseObject['statusCode'] = 200
    responseObject['headers'] = {}
    responseObject['headers']['Content-type'] = 'application/json'
    responseObject['body'] = json.dumps(transactionResponse)

    #5. Return the response object
    return responseObject
    

def checkInt(x):
    try:
        int(x)
        return True
    except ValueError:
        return False
        
def getPlayer(dyndb, playerName):
    response = dyndb.get_item(
    TableName = 'prPlayers',
    Key={
        'name':{'S':playerName}
        }
    )
    return response
    
def upsertPlayer(dyndb, playerName, numOfGames, numOfWins):
    item = {}
    item['name'] = {'S':playerName}
    item['numOfGames'] = {'N':numOfGames}
    item['numOfWins'] = {'N':numOfWins}
    response = dyndb.put_item(
        TableName = 'prPlayers',
        Item = item)
    return response
    
def deletePlayer(dyndb, playerName):
    response = dyndb.delete_item(
        TableName = 'prPlayers',
        Key = {
            'name':{'S':playerName}
            }
    ) 
    return response