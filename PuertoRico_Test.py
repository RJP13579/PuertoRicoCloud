"""
   Tiny Version of Puerto Rico.
   All it does is read the game setup JSON and output a simplar JSON.
   
   Vastly simplified Puerto Rico to enter the Serverless world

"""


import Func_WriteLog
import random
import sys
import os.path
import json

def PuertoRico(f, numOfPlayers, playerNames):
    scores = [30, 35, 40, 45, 50]
    f.write("--------\n New Game of Puerto Rico! \n\n")
    print("\nNew game of Puerto Rico.........")
    for i in playerNames:
        f.write("{}'s score is {}.  ".format(i, random.choice(scores)))
        print("{}'s score is {}".format(i, random.choice(scores)))

    f.write("\n")
    

if __name__ == '__main__':
    # Open an empty LogFile.txt
    f = open(os.path.join('FlatFiles','log_file.txt'), mode = 'w', encoding = 'utf-8')

    # Read numOfPlayers and playerNames from GameSetUp.json 
    # loop through as many PuertoRico games as are nessecary
    with open(os.path.join('FlatFiles','GameSetUp.json'), mode = 'r', encoding = 'utf-8') as setUpFile:
        for line in setUpFile:
            gameSetup = json.loads(line)
            PuertoRico(f, gameSetup['numOfPlayers'], gameSetup['playerNames'])
    f.close()