import json
import boto3

# Create SQS client
sqs = boto3.client('sqs')

def lambda_handler(event, context):
    queue_url = 'https://sqs.eu-west-1.amazonaws.com/653875496653/prGameQueue'

    # Receive message from SQS queue
    response = sqs.receive_message(
        QueueUrl=queue_url,
        AttributeNames=[
            'SentTimestamp'
        ],
        MaxNumberOfMessages=3,
        MessageAttributeNames=[
            'All'
        ],
        VisibilityTimeout=0,
        WaitTimeSeconds=0
    )

    status = 'Message Recieved'
    print(status)
    everything = response
    fullMessage = response['Messages'][0]
    payload = fullMessage['MessageAttributes']
    receipt_handle = fullMessage['ReceiptHandle']

    # Delete received message from queue
    sqs.delete_message(
        QueueUrl=queue_url,
        ReceiptHandle=receipt_handle
    )
    status = status + ' and Deleted'
    print(status)
    print('\n------------')
    print('everything is: {}'.format(everything))
    print('fullMessage is: {}'.format(fullMessage))
    print('payload is: {}'.format(payload))
    print('receipt_handle is: {}'.format(receipt_handle))
    print('------------')
    
    
    #3. Construct the body of the response object
    transactionResponse = {}
    transactionResponse['status'] = status
    transactionResponse['Everything'] = everything
    transactionResponse['Full Message'] = fullMessage
    transactionResponse['Payload'] = payload

    #4. Construct http response object
    responseObject = {}
    responseObject['statusCode'] = 200
    responseObject['headers'] = {}
    responseObject['headers']['Content-type'] = 'application/json'
    responseObject['body'] = json.dumps(transactionResponse)

    #5. Return the response object
    return responseObject