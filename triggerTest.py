"""
Small Lambda function to respond to being triggered by SQS
"""
import json
import boto3
from ast import literal_eval

sqs = boto3.client('sqs')
s3 = boto3.resource("s3")

def lambda_handler(event, context):

    fullMessage = event['Records'][0]
    sqsPayload = fullMessage['messageAttributes']
    playerNames = literal_eval(sqsPayload['playerList']['stringValue'])
    numOfPlayers = len(playerNames)
    receipt_handle = fullMessage['receiptHandle']

    # -----------------------------------------------
    # 3. Write the output to s3
    string = json.dumps(playerNames)
    string = string + ' Total players are {}'.format(len(playerNames))
    encoded_string = string.encode("utf-8")
    bucket_name = 'pr-outputdata'
    file_name = 'prOutputLog.txt'
    lambda_path = 'tmp/' + file_name
    s3_path = 'output' + file_name
    s3.Bucket(bucket_name).put_object(Key = s3_path, Body = encoded_string)

    
    # -----------------------------------------------
    #4. Construct the body of the response object
    transactionResponse = {}
    transactionResponse['event'] = event

    # -----------------------------------------------
    #5. Construct http response object
    responseObject = {}
    responseObject['statusCode'] = 200
    responseObject['headers'] = {}
    responseObject['headers']['Content-type'] = 'application/json'
    responseObject['body'] = string

    # -----------------------------------------------
    #5. Return the response object
    return responseObject
    
    