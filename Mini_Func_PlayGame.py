"""
Mini Puerto Rico function
"""

def PlayGame(allPlayers):
    finalScores = {}
    for i in allPlayers:
        i.assignScore()
        finalScores[i.name] = i.score
    
    return finalScores
        

    