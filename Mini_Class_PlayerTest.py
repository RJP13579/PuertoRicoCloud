"""
Simplified Player class for testing
"""
import random

class Player():

    def __init__(self, name):
        self.name = name
        self.score = 0

    def assignScore(self):
        scores = [x+1 for x in range (25, 55)]
        self.score = random.choice(scores)

    def scoreDict(self):
        scoreDict = {}
        d['self.name'] = self.score
        return scoreDict

